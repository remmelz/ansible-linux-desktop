#!/usr/bin/python3

import os
import sys
import re

f=sys.argv[-1]
f = f.replace('\\','')

special_char = re.compile('[ @!#$%^&*()<>?/\|}{~:]')

if len(sys.argv) == 1: sys.exit(1)
if not os.path.isfile(f): sys.exit(1)
if (special_char.search(f) == None): sys.exit(1)

ext=''
if '.' in f:
  ext = '.'+f.split('.')[-1]

newname = ''
for s in f.split(' '):
  newname += ''.join(filter(str.isalnum, s))+'_'

if len(ext) > 0:
  newname = newname[:-(len(ext))]+ext
else:
  newname = newname[:-1]

f = f.replace("'", "'\\''")
os.system("mv -v '''"+f+"''' "+newname)


