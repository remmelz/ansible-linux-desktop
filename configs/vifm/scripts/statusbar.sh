#!/bin/bash

printf "$(df -h . | tail -1 | awk -F' ' '{print $1" "$3"/"$2}'), "
printf "$(du -hs . | awk -F' ' '{print $1}')\n"

