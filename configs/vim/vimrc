"============================================================
" Plugins (disabled by default)
"============================================================

"call plug#begin()
"  Plug 'junegunn/vim-easy-align'
"  Plug 'dhruvasagar/vim-table-mode'
"call plug#end()

"============================================================
" GVIM Settings (disabled by default)
"============================================================

"set guifont=Lucida_Console:h10:cANSI:qDRAFT  " Font settings
"set lines=50 columns=120                     " Window size
"set guioptions-=T                            " Disable toolbar
"set guioptions-=m                            " Disable dropdown menu
"set clipboard=unnamed                        " Set clipboard
"set vb t_vb=                                 " Disable (visual)bell
"
"if isdirectory('C:\Notes')
" cd C:\Notes
"endif

"============================================================
" General
"============================================================

" Editing
set tabstop=2        " The width of a TAB is set to 2.
set shiftwidth=2     " Indents will have a width of 2.
set softtabstop=2    " Sets the number of columns for a TAB.
set expandtab        " Expand TABs to spaces.
set nowrap           " do not automatically wrap on load
set formatoptions-=t " do not automatically wrap text when typing
set number           " Sets linenumbers
set noequalalways    " This will cause vim to size each new split relative the current split
set nolist           " Hide invisible characters
set autoindent       " Useful for structured text files
set ignorecase       " When searching ignore case sensitivity
set smartcase        " Enable case sensitivity only when using uppercase characters
set splitright       " When splitting, open on right side
"set relativenumber  " Sets relative linenumbers
"set mouse+=a        " Add mouse support


"============================================================
" Theme
"============================================================

" Enable Syntax Highlight
syntax on

" Cosmetics
colorscheme elflord
highlight LineNr      ctermfg=darkgrey guifg=darkgrey
highlight ColorColumn ctermbg=darkgrey guibg=darkgrey

"============================================================
" Text Wrap
"============================================================
 
function! ToggleTextWrap()
  if &textwidth == 0
    set formatoptions+=t
    set textwidth=79
    set colorcolumn=80
  else
    set formatoptions-=t
    set textwidth=0
    set colorcolumn=0
  endif
endfunction

command ToggleTextWrap :call ToggleTextWrap()

"============================================================
" Keyboard Shortcuts
"============================================================

" File navigation
nnoremap <C-e> :Explore<enter>

" Window management
nnoremap \| :vsplit .<enter>
nnoremap <  :vertical resize -10<enter>
nnoremap >  :vertical resize +10<enter>
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Tab management
nnoremap tn    :tabnew<enter>
nnoremap tc    :tabclose<enter>
nnoremap <C-m> :tabnext<enter>
nnoremap <C-n> :tabprevious<enter>

" Switch to visual mode
imap     fj <Esc>
imap     fk <Esc>

" Saving
nnoremap <C-x> :x!<enter>

" Textwrap
nnoremap tw :ToggleTextWrap<enter>

" Tablemode
nnoremap tt :TableModeToggle<enter>

" EasyAlign
vnoremap af :EasyAlign 1/[:;\|=,]/<enter>  " Align first
vnoremap aa :EasyAlign */[:;\|=,]/<enter>  " Align all
vnoremap as :EasyAlign */[\ ]/<enter>      " Align spaces
vnoremap ap :EasyAlign */[\|]/<enter>      " Align pipes

"============================================================
" EOF
"============================================================


