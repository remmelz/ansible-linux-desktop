#!/bin/bash

#
# nvidia-settings --write-config
#

cd /etc/X11 | exit 1

if [[ -f xorg.conf ]]
then
  mv -v xorg.conf xorg.conf.nvidia
else
  if [[ -f xorg.conf.nvidia ]]
  then
    mv -v xorg.conf.nvidia xorg.conf
  fi
fi

