#!/bin/bash
#
# Find Libreoffice documents
#
####################################

cd ~/Documents || exit 1

doc=$(find . -maxdepth 4 -type f \
  \( -name "*.odt" -o -name "*.ods" -o -name "*.odg" \) \
  | sort | fzf --layout=reverse)

if [[ -f "${doc}" ]]
then
  libreoffice "${doc}" > /dev/null 2>&1
fi

exit 0

