#!/bin/bash

_appname=$1
_pubkey=$2

if [[ $1 == '--help' ]]
then
  echo "Usage: $0 <appname> <user_who_wants_to_use_app>"
  exit 1
fi

if [[ -z ${_appname} ]]
then
  echo "Error: no appname provided."
  exit 1
fi

if [[ -z ${_pubkey} ]]
then
  echo "Error: username for ssh pubkey given."
  exit 1
fi

if [[ -z $(cat /etc/ssh/sshd_config | grep X11Forwarding | grep yes) ]]
then
  echo "Error: X11Forwarding is not set to yes."
  exit 1
fi

if [[ ! -f /home/${_pubkey}/.ssh/id_rsa.pub ]]
then
  echo "Error: Could not find id_rsa.pub for ${_pubkey}."
  exit 1
fi

if [[ ! -d /home/${_appname} ]]
then
    useradd -m ${_appname} || exit 1

    mkdir -v /home/${_appname}/.ssh
    mkdir -v /home/${_appname}/app
    cat /home/${_pubkey}/.ssh/id_rsa.pub > /home/${_appname}/.ssh/authorized_keys

    chown -v ${_appname}: /home/${_appname}/.ssh/
    chown -v ${_appname}: /home/${_appname}/.ssh/authorized_keys
    chown -v ${_appname}: /home/${_appname}/app/

    chmod -v 750 /home/${_appname}
    chmod -v 750 /home/${_appname}/app
    chmod -v 750 /home/${_appname}/.ssh
    chmod -v 600 /home/${_appname}/.ssh/authorized_keys
fi

if [[ $(ls -l /home/ \
  | grep -v 'lost+found' \
  | grep -v 'drwxr-x---' \
  | wc -l) -gt 1 ]]
then
  echo "Error: ensure home folder permissions correct."
  exit 1
fi

echo "Environment ready."
echo "Copy contents to ~/.local/share/applications/${_appname}.desktop"
echo
echo "[Desktop Entry]"
echo "Name=${_appname}"
echo "Comment=${_appname}"
echo "Type=Application"
echo "Exec=ssh -XY ${_appname}@localhost ${_appname}"
echo "Categories=Internet"
echo


