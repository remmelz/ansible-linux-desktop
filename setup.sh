#!/bin/bash

_inventory=$1
shift
_args=$@

#####################################################
# Sanity check
#####################################################

which ansible-playbook

if [[ $? != 0 ]]; then
  echo 'Error: one or more required dependencies are missing.'
  exit 1
fi


#####################################################
# Show help
#####################################################

if [[ -z ${_inventory} ]]; then
  echo "Usage: $0 <inventory>"
  echo
  echo '  -vvv                                   verbose level'
  echo '  --tags       "packages,configs,skel"   run only tags'
  echo '  --extra-vars "hardware=yes"            install packages when on hardware'
  echo
  exit 1
fi


#####################################################
# Start Playbook
#####################################################

ansible-playbook \
  ${_args} \
  -i ${_inventory} \
  playbook.yml


